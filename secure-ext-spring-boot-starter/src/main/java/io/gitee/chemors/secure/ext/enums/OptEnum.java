/*
 * Copyright (c)  小尘哥. 2022-2024. All rights reserved.
 */

package io.gitee.chemors.secure.ext.enums;

/**
 * 选择枚举
 *
 * @author 小尘哥
 */
public enum OptEnum {

    OPT_STRING;
}
