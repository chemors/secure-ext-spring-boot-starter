/*
 * Copyright (c)  小尘哥. 2022-2024. All rights reserved.
 */

package io.gitee.chemors.secure.ext.config;

/**
 * 常量
 *
 * @author 小尘哥
 */
public class Constants {

    public static final String LOG_DELIM="\\{\\}";

    public static final String PACKAGES_SPLIT=",";

    public static final String LOG_STR_SPLIT="^";

    public static final String REG_LOG_STR_SPLIT = "\\" + LOG_STR_SPLIT;


}
